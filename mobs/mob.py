import random
class Mob():
    def __init__(self):
        self.health = 0
        self.description = "An empty mob"
        self.attack = 0
        self.defense = 0
        self.name = 0
        self.race = "Null"
        self.ID = 0 
        self.room_location = -1
        self.faction = "Null"
        self.drops = None


class Player(Mob):
    inventory = []
    equipped = {}
    def __init__(self, h = 10, a = 5, d = 5, n = "cat * | grep 'No Name'", 
            r = "Human", i = 0, l = 0, f = "Wintreath", desc = "A Player"):
        self.health = h
        self.attack = a
        self.defense = d
        self.name = n
        self.race = r 
        self.faction = f
        random.seed(i + r + f + n)
        self.ID = str(i + r + f + n + random.randint()) 
        self.room_location = l
        self.faction = f
        self.description = desc
        self.drops = inventory

    def spawn(l = 0):
        self.room_location = l
        

    def talk(speech):
        return (self.name + ": " + speech)

    def death(player):
        inventory = []
        equipped = {}
        return player, self.drops

class Slime(Mob):
    def __init__(self):
        self.health = 1
        self.attack = 1
        self.defense = 0
        self.name = "Slime"
        self.ID = 0
        self.description = "A green gooey slime"
        self.drops = None

    def spawn(l = 1):
        self.room_location = l

    def death(player):
        return player, self.drops
    
