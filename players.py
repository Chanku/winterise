import sqlite3

#
# This function checks for a specific player name (or ID?)
# The data returned is True or False/None. 
# Please note that player_name is NOT sanitized.
#
def check_for_player(player_name):
    print("NOT IMPLIMENTED YET")

#
# This takes the player_name from an existing player 
# and it then returns the player's data.
#
# Please note that player_name is NOT sanitized.
#
def load_player(player_name):
    print("NOT IMPLIMENTED YET!")

#
# This removes a player from the database. Please note 
#   that this returns True if it is successful and 
#   false if it is not.
#
# Please note that player_name is not sanitized.
#
def delete_player(player_name):
    print("NOT IMPLIMENTED YET!")
    
#
# This adds a player to the databse. Please note that 
#   this function does not return anything.
#
# Please note that player_name is not sanitized
#
def create_player(player_name, player_data):
    print("NOT IMPLIMENTED YET!")
